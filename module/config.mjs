export const CONFIG = {};

CONFIG.ID = "core-settings-expanded";

CONFIG.FOUNDRYVERSION = 0;

CONFIG.SETTINGS = {
    InfoButton: "Info Button",
    Settings: "Settings"
}

CONFIG.TEMPLATES = {
    PARTIALS: {
        Core: "/modules/core-settings-expanded/templates/partials/core.hbs",
        MapNote: "/modules/core-settings-expanded/templates/partials/mapnote.hbs",
        Scene: "/modules/core-settings-expanded/templates/partials/scene.hbs",
        Journal: "/modules/core-settings-expanded/templates/partials/journal.hbs"
    },
    Settings: "/modules/core-settings-expanded/templates/settings-dialog.hbs"
};

CONFIG.ActiveJournals = { };

CONFIG.COREEXPANDED = { };