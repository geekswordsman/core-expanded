import { Logger } from "./logger/logger.mjs";
import { CONFIG } from "./config.mjs";

export class API {
    /**
     * MassUpdateMapNoteBackgroundTint
     * API to mass update notes with Core Settings Expanded background tint option; either enabling or disabling it and either the current scene or all scenes in the sidebar.
     * @param {*} options Accepts two optional paremeters
     *  @param {bool} tintEnabled Should the background tint be enabled, default true
     *  @param {bool} allScenes Should all scenes be affected or just the current scene.  Default false (current scene only)
     * @returns Promise(bool); true on successful update and false on error.
     * Example usage to mass update all mapnotes in all scenes to set have the background tint.
     * await game.modules.get("core-settings-expanded").api.MassUpdateMapNoteBackgroundTint({tintEnabled: true, allScenes:true});
     */
    static async MassUpdateMapNoteBackgroundTint(options={tintEnabled:true, allScenes:false}) {
        let scenes = [];
        try {
            if (options.allScenes) {
                scenes = scenes.concat(game.scenes.contents);
            } else {
                scenes.push(canvas.scene);
            }

            for (let scene of scenes) {
                const mapNotes = scene.notes.contents;
                for (let note of mapNotes) {
                    await note.setFlag(CONFIG.ID, "hasBackground", options.tintEnabled);
                }
            }
        } catch (e) {
            Logger.error(true, "Core Settings Expanded API Error", e);
            return false;
        }

        return true;
    }
}