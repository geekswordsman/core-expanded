import { Logger } from "./logger/logger.mjs";
import { CONFIG } from "./config.mjs";
import { AboutDialog } from "./about/about-dialog.mjs";
import { SettingsDialog } from "./dialogs/SettingsDialog.mjs";
import { CoreExpandedControlIcon } from "./subclasses/CoreExpandedControlIcon.mjs";
import { API } from "./api.mjs";

export class CoreExpanded {
    static get DefaultOptions() {
        return {
            Core: {
                FilePicker: {
                    alwaysUseDefaultDirectory: false,
                    defaultDirectory: null,
                    alwaysUseDisplayMode: false,
                    displayMode: null
                },
                StatusIcons: {
                    newIcons: [],
                    disabled: [],
                    default: deepClone(window.CONFIG.statusEffects)
                },
                Templates: {
                    Cone: {
                        enableOverride: false,
                        overrideAngle: 53.13
                    },
                    Ray: {
                        enableOverride: false,
                        overrideWidth: 1
                    }
                },
                Tokens: {
                    overrideSpeed: false,
                    MovementSpeed: 6
                }
            },
            Scene: {
                EnableSceneOverride: false,
                BackgroundColor: "#999999",
                GridSize: 100,
                GridType: "Square",
                PaddingPercentage: 0.25,
                GridScale: {
                    distance: 5,
                    units: "ft"
                },
                GridColor: "#000000",
                GridOpacity: 0.2,
                TokenVision: true,
                FogExploration: true,
                UnrestrictedVisionRange: false,
                DarknessLevel: 0,
                VisionLimitationEnabled: false,
                VisionLimitationThreshold: 0
            },
            MapNote: {
                DefaultFont: null,
                DefaultIcon: null,
                EnableBackgroundTintDisabling: false,
                EnableOverride: false,
                FontSize: null,
                Icons: {
                    newIcons: [],
                    disabled: [],
                    default: {"Anchor":"icons/svg/anchor.svg","Barrel":"icons/svg/barrel.svg","Book":"icons/svg/book.svg","Bridge":"icons/svg/bridge.svg","Cave":"icons/svg/cave.svg","Castle":"icons/svg/castle.svg","Chest":"icons/svg/chest.svg","City":"icons/svg/city.svg","Coins":"icons/svg/coins.svg","Fire":"icons/svg/fire.svg","Hanging Sign":"icons/svg/hanging-sign.svg","House":"icons/svg/house.svg","Mountain":"icons/svg/mountain.svg","Oak Tree":"icons/svg/oak.svg","Obelisk":"icons/svg/obelisk.svg","Pawprint":"icons/svg/pawprint.svg","Ruins":"icons/svg/ruins.svg","Tankard":"icons/svg/tankard.svg","Temple":"icons/svg/temple.svg","Tower":"icons/svg/tower.svg","Trap":"icons/svg/trap.svg","Skull":"icons/svg/skull.svg","Statue":"icons/svg/statue.svg","Sword":"icons/svg/sword.svg","Village":"icons/svg/village.svg","Waterfall":"icons/svg/waterfall.svg","Windmill":"icons/svg/windmill.svg"}
                },
                IconSize: 40,
                IconTint: null,
                TextColor: "#FFFFFF",
                TextAnchorPoint: "Bottom"
            },
            Journal: {
                DefaultView: 1,
                OverrideEnabled: false,
                SideBarCollapsed: false    
            }
        };
    }

    /**
     * initialize - Register game settings
     */
    static initialize() {
        Logger.info(true, `Initializing ${game.i18n.localize("CORE-EXPANDED.ABOUT.Title")}!`);
        AboutDialog.TITLE = game.i18n.localize("CORE-EXPANDED.ABOUT.Title");

        CONFIG.FOUNDRYVERSION = game.version ?? game.data.version;

        /* Settings Registration */
        game.settings.register(CONFIG.ID, CONFIG.SETTINGS.Settings, {
            restricted: true,
            scope: 'world',
            type: Object,
            default: CoreExpanded.DefaultOptions
        });

        /* Menu Registrations */
        game.settings.registerMenu(CONFIG.ID, CONFIG.SETTINGS.InfoButton, {
            name: game.i18n.localize("CORE-EXPANDED.ABOUT.About"),
            label: game.i18n.localize("CORE-EXPANDED.ABOUT.About"),
            restricted: false,
            icon: 'fas fa-info-circle',
            type: AboutDialog
        });

        Note.prototype._drawControlIcon = function() {
            let tint = null;
            let iconData = null;
        
            if (game.release?.generation >= 11) {
                tint = this.document.texture.tint ? colorStringToHex(this.document.texture.tint) : null;
                iconData = { texture: this.document.texture.src, size: this.size, tint: tint };
            } else if (game.release?.generation >= 10) {
                tint = this.document.iconTint ? colorStringToHex(this.document.iconTint) : null;
                iconData = { texture: this.document.texture.src, size: this.size, tint: tint };
            } else {
                tint = this.data.iconTint ? colorStringToHex(this.data.iconTint) : null;
                iconData = { texture: this.data.icon, size: this.size, tint: tint };
            }
            let icon;
            const coreExpandedSettings = game.settings.get("core-settings-expanded", "Settings");
            if (coreExpandedSettings?.MapNote?.EnableBackgroundTintDisabling && !this.document.getFlag(CONFIG.ID, "hasBackground")) {
                icon = new CoreExpandedControlIcon(iconData);
            } else {
                icon = new ControlIcon(iconData);
            }
            icon.x -= this.size / 2;
            icon.y -= this.size / 2;
            return icon;            
        };

        return CoreExpanded.preloadHandlebarTemplates();
    }

    /**
     * onReady
     */
    static async onReady() {
        CONFIG.COREEXPANDED = mergeObject(CoreExpanded.DefaultOptions, game.settings.get(CONFIG.ID, CONFIG.SETTINGS.Settings));
        game.modules.get(CONFIG.ID).api = API;

        if (!CONFIG.COREEXPANDED.Core?.Tokens) {
            CONFIG.COREEXPANDED.Core.Tokens = {
                overrideSpeed: false,
                MovementSpeed: 6
            }            
        }

        //Merge & Update the default status icons - systems might have updated these!
        mergeObject(CONFIG.COREEXPANDED.Core.StatusIcons.default, deepClone(window.CONFIG.statusEffects));
        await game.settings.set(CONFIG.ID, CONFIG.SETTINGS.Settings, CONFIG.COREEXPANDED);

        if (CONFIG.COREEXPANDED.Core?.StatusIcons?.disabled) {
            let removeIds = CONFIG.COREEXPANDED.Core.StatusIcons.disabled.map((s) => { return s.id; });
            window.CONFIG.statusEffects = window.CONFIG.statusEffects.filter(s => !removeIds.includes(s.id))
        }

        if (CONFIG.COREEXPANDED.Core?.StatusIcons?.newIcons) {
            mergeObject(window.CONFIG.statusEffects, CONFIG.COREEXPANDED.Core.StatusIcons.newIcons);
            //window.CONFIG.statusEffects = window.CONFIG.statusEffects.concat(CONFIG.COREEXPANDED.Core.StatusIcons.newIcons);
        }

        if (CONFIG.COREEXPANDED?.Core?.FilePicker?.displayMode) {
            FilePicker.LAST_DISPLAY_MODE = CONFIG.COREEXPANDED.Core.FilePicker.displayMode;
        }

        if (CONFIG.COREEXPANDED?.Core?.FilePicker?.defaultDirectory) {
            FilePicker.LAST_BROWSED_DIRECTORY = CONFIG.COREEXPANDED.Core.FilePicker.defaultDirectory;
        }

        if (CONFIG.COREEXPANDED?.MapNote?.Icons?.disabled) {
            let removeIds = CONFIG.COREEXPANDED.MapNote.Icons.disabled.map((s) => { return s.name; });
            removeIds.forEach(i => delete(window.CONFIG.JournalEntry.noteIcons[i]));
        }

        if (CONFIG.COREEXPANDED?.MapNote?.Icons?.newIcons) {
            for (let icon of CONFIG.COREEXPANDED.MapNote.Icons.newIcons) {
                window.CONFIG.JournalEntry.noteIcons[icon.name] = icon.icon;
            }
        }

        if (!CONFIG.COREEXPANDED.Journal) {
            CONFIG.COREEXPANDED.Journal = {
                DefaultView: 1,
                OverrideEnabled: false,
                SideBarCollapsed: false
            }
        }

        /* File Picker Override */
        Hooks.on("renderFilePicker", (app, html, options) => { CoreExpanded.setupFilePicker(app, html, options); });
        //Hooks.on("closeFilePicker", (app, html, options) => { CoreExpanded.setupFilePicker(app, html, options); });
    }

    static async preloadHandlebarTemplates() {
        const partials = Object.keys(CONFIG.TEMPLATES.PARTIALS).map((k) => { return CONFIG.TEMPLATES.PARTIALS[k] });
        return loadTemplates(partials);
    }

    static setupOverrides() {
        if (CONFIG.COREEXPANDED.Core?.Templates?.Cone?.enableOverride && CONFIG.COREEXPANDED.Core?.Templates?.Cone?.overrideAngle) {
            window.CONFIG.MeasuredTemplate.defaults.angle = CONFIG.COREEXPANDED.Core.Templates.Cone.overrideAngle;
        }
        if (CONFIG.COREEXPANDED.Core?.Templates?.Ray?.enableOverride && CONFIG.COREEXPANDED.Core?.Templates?.Ray?.overrideWidth) {
            window.CONFIG.MeasuredTemplate.defaults.width = CONFIG.COREEXPANDED.Core.Templates.Ray.overrideWidth;
        }
    }

    /**
     * renderSettingsSidebar - Add the Core Expander button
     * @param {*} app - Foundry class for the sidebar
     * @param {*} html - the HTML of the sidebar
     * @returns 
     */
     static async renderSettingsSidebar(app, html) {
        if (app.options.id !== "settings" || !game.user.isGM) { return; }

        let button = document.createElement("button");
        button.setAttribute("type", "button");
        button.dataset["action"] = "core-expanded";
        button.innerHTML = `<i class="fas fa-expand"></i>${game.i18n.localize("CORE-EXPANDED.SETTINGS.Title")}`;
        button.onclick = () => { const settings = new SettingsDialog(); settings.render(true); }

        let settingsButton = html.find("button[data-action='configure'");
        $(button).insertAfter(settingsButton);
     }

     /**
      * setupFilePicker - Adjusts the File Picker options if overrideen
      * @param {*} app 
      * @param {*} html 
      * @param {*} options 
      */
     static async setupFilePicker(app, html, options) {
        if (app["coreSettingsExpandedDisplayID"] === app.appId || (!CONFIG.COREEXPANDED?.Core?.FilePicker?.alwaysUseDefaultDirectory && !CONFIG.COREEXPANDED?.Core?.FilePicker?.alwaysUseDisplayMode)) {
            return;
        }

        let target = null;
        options = options ?? {};

        if (CONFIG.COREEXPANDED?.Core?.FilePicker?.defaultDirectory) {
            target = options["activeSource"] = FilePicker.LAST_BROWSED_DIRECTORY = CONFIG.COREEXPANDED.Core.FilePicker.defaultDirectory;
        }

        if (CONFIG.COREEXPANDED?.Core?.FilePicker?.displayMode) {
            app.displayMode = options["displayMode"] = FilePicker.LAST_DISPLAY_MODE = CONFIG.COREEXPANDED.Core.FilePicker.displayMode;
        }

        Logger.debug(false, "File Picker Setup", app, html, options);

        await app.browse(target, options);
       
        app["coreSettingsExpandedDisplayID"] = app.appId;
     }

     /**
      * preCreateScene - Sets default scene parameters
      * @param {*} scene 
      * @param {*} params 
      * @param {*} options 
      * @param {*} sceneID 
     */
     static async preCreateScene(scene, params, options, sceneID) {
        if (!CONFIG.COREEXPANDED?.Scene?.EnableSceneOverride) { return; }
        if (options.fromCompendium) { return; } //Fixes #10 - Don't apply settings to imported scenes!

        let update = { };

        Logger.debug(false, "Pre Create Scene!", scene, params, options, sceneID, update);
        if (isNewerVersion(CONFIG.FOUNDRYVERSION, "10")) {
            update = {
                backgroundColor: CONFIG.COREEXPANDED.Scene.BackgroundColor,
                grid: {
                    size: CONFIG.COREEXPANDED.Scene.GridSize,
                    type: CONFIG.COREEXPANDED.Scene.GridType,
                    distance: CONFIG.COREEXPANDED.Scene.GridScale.distance,
                    units: CONFIG.COREEXPANDED.Scene.GridScale.units,
                    color: CONFIG.COREEXPANDED.Scene.GridColor,
                    alpha: CONFIG.COREEXPANDED.Scene.GridOpacity,
                },
                padding: CONFIG.COREEXPANDED.Scene.PaddingPercentage,
                tokenVision: CONFIG.COREEXPANDED.Scene.TokenVision,
                fogExploration: CONFIG.COREEXPANDED.Scene.FogExploration,
                globalLight: CONFIG.COREEXPANDED.Scene.UnrestrictedVisionRange,
                darkness: CONFIG.COREEXPANDED.Scene.DarknessLevel,
                globalLightThreshold: CONFIG.COREEXPANDED.Scene.VisionLimitationEnabled ? CONFIG.COREEXPANDED.Scene.VisionLimitationThreshold : null
            }

            await scene.updateSource(update);
        } else {
            update = {
                backgroundColor: CONFIG.COREEXPANDED.Scene.BackgroundColor,
                grid: CONFIG.COREEXPANDED.Scene.GridSize,
                padding: CONFIG.COREEXPANDED.Scene.PaddingPercentage,
                gridDistance: CONFIG.COREEXPANDED.Scene.GridScale.distance,
                gridUnits: CONFIG.COREEXPANDED.Scene.GridScale.units,
                gridColor: CONFIG.COREEXPANDED.Scene.GridColor,
                gridAlpha: CONFIG.COREEXPANDED.Scene.GridOpacity,
                tokenVision: CONFIG.COREEXPANDED.Scene.TokenVision,
                fogExploration: CONFIG.COREEXPANDED.Scene.FogExploration,
                globalLight: CONFIG.COREEXPANDED.Scene.UnrestrictedVisionRange,
                darkness: CONFIG.COREEXPANDED.Scene.DarknessLevel,
                globalLightThreshold: CONFIG.COREEXPANDED.Scene.VisionLimitationEnabled ? CONFIG.COREEXPANDED.Scene.VisionLimitationThreshold : null
            }

            await scene.data.update(update);
        }
     }

     /**
      * createMapNote - Sets default parameters for this map note
      * @param {*} app 
      * @param {*} html 
      * @param {*} data 
      */
     static async createMapNote(app, html, data) {
        if (app.coreExpandedUpdated) { return; }
        Logger.debug(false, "Create Map Note!", app, html, data);

        if (!CONFIG.COREEXPANDED?.MapNote?.EnableOverride) { return; }

        let update = { };

        if (isNewerVersion(CONFIG.FOUNDRYVERSION, "11")) {
            update.texture = { 
                src: window.CONFIG.JournalEntry.noteIcons[CONFIG.COREEXPANDED.MapNote.DefaultIcon] ?? data.document.texture.src,
                tint:  CONFIG.COREEXPANDED.MapNote.IconTint !== null && CONFIG.COREEXPANDED.MapNote.IconTint !== "" ? CONFIG.COREEXPANDED.MapNote.IconTint : data.document.texture.tint
            };
            update.fontFamily = CONFIG.COREEXPANDED.MapNote.DefaultFont ?? data.document.fontFamily;
            update.fontSize = CONFIG.COREEXPANDED.MapNote.FontSize ?? data.document.fontSize;
            update.iconSize = CONFIG.COREEXPANDED.MapNote.IconSize ?? data.document.iconSize;
            update.textAnchor = CONFIG.COREEXPANDED.MapNote.TextAnchorPoint ?? data.document.textAnchor;
            update.textColor = CONFIG.COREEXPANDED.MapNote.TextColor ?? data.document.textColor;
        } else if (isNewerVersion(CONFIG.FOUNDRYVERSION, "10")) {
            update.texture = { src: window.CONFIG.JournalEntry.noteIcons[CONFIG.COREEXPANDED.MapNote.DefaultIcon] ?? data.document.texture.src };
            update.fontFamily = CONFIG.COREEXPANDED.MapNote.DefaultFont ?? data.document.fontFamily;
            update.fontSize = CONFIG.COREEXPANDED.MapNote.FontSize ?? data.document.fontSize;
            update.iconSize = CONFIG.COREEXPANDED.MapNote.IconSize ?? data.document.iconSize;
            update.iconTint = CONFIG.COREEXPANDED.MapNote.IconTint ?? data.document.iconTint;
            update.textAnchor = CONFIG.COREEXPANDED.MapNote.TextAnchorPoint ?? data.document.textAnchor;
            update.textColor = CONFIG.COREEXPANDED.MapNote.TextColor ?? data.document.textColor;
        } else {
            update.icon = window.CONFIG.JournalEntry.noteIcons[CONFIG.COREEXPANDED.MapNote.DefaultIcon] ?? data.data.icon;
            update.fontFamily = CONFIG.COREEXPANDED.MapNote.DefaultFont ?? data.data.fontFamily;
            update.fontSize = CONFIG.COREEXPANDED.MapNote.FontSize ?? data.data.fontSize;
            update.iconSize = CONFIG.COREEXPANDED.MapNote.IconSize ?? data.data.iconSize;
            update.iconTint = CONFIG.COREEXPANDED.MapNote.IconTint ?? data.data.iconTint;
            update.textAnchor = CONFIG.COREEXPANDED.MapNote.TextAnchorPoint ?? data.data.textAnchor;
            update.textColor = CONFIG.COREEXPANDED.MapNote.TextColor ?? data.data.textColor;
        }

        app.coreExpandedUpdated = true;
        if(isNewerVersion(CONFIG.FOUNDRYVERSION, "10")) {
            await app.document.updateSource(update);
        } else {
            await app.document.data.update(update);
        }
        await app.render();
     }

     static preUpdateToken(tokenDoc, update, options) {
        if (!CONFIG.COREEXPANDED?.Core?.Tokens?.overrideSpeed) { return; }

        const speed = CONFIG.COREEXPANDED.Core.Tokens.MovementSpeed;
        const x = foundry.utils.hasProperty(update, "x");
        const y = foundry.utils.hasProperty(update, "y");
        if ( !x && !y ) return;
        foundry.utils.setProperty(options, "animation.movementSpeed", speed);
     }

     static renderNoteConfig(noteConfig, html, data) {
        if (!CONFIG.COREEXPANDED?.MapNote.EnableBackgroundTintDisabling) { return; }

        const hasBackground = noteConfig.object.getFlag(CONFIG.ID, "hasBackground") ?? false;
        let iconTintGroup = null;
        if (game.release?.generation >= 10) {
            iconTintGroup = html.find("[name='texture.tint']").closest(".form-group");
        } else {
            iconTintGroup = html.find("[name=iconTint]").closest(".form-group");
        }

        iconTintGroup.after(`
            <div class="form-group">
                <label for="flags.${CONFIG.ID}.hasBackground">Show Background?</label>
                <input type="checkbox" name="flags.${CONFIG.ID}.hasBackground" data-dtype="Boolean" ${hasBackground ? "checked" : ""}>
            </div>
        `);

        noteConfig.setPosition({ height: "auto" });
     }

     static async renderJournalSheet(app, html, data) {
        if (!CONFIG.COREEXPANDED?.Journal?.OverrideEnabled || !isNewerVersion(CONFIG.FOUNDRYVERSION, "10")) { return; }
        Logger.debug(false, "Render JournalSheet", app, html, data);
        

        if (Object.keys(CONFIG.ActiveJournals).find(a => a === app.object._id) === undefined) {
            CONFIG.ActiveJournals[app.object._id] = { };
            const entry = app.document;
            const currentMode = entry.getFlag("core", "viewMode");
            const defaultMode = CONFIG.COREEXPANDED?.Journal.DefaultView;
            if ( currentMode !== defaultMode ) {
                entry.setFlag("core", "viewMode", defaultMode);
            } else if (app.mode !== currentMode) {
                await app.close(true);
                let renderPromise = app._onAction({
                    preventDefault: () => {},
                    currentTarget: {
                        dataset: {
                            action: "toggleView"
                        }
                    }
                });

                if (renderPromise instanceof Promise) { await renderPromise; }
            }
        }

        if (app.sidebarCollapsed !== CONFIG.COREEXPANDED?.Journal?.SideBarCollapsed) {
            app.toggleSidebar();
        }
     }

     static async closeJournalSheet(app, data) {
        delete CONFIG.ActiveJournals[app.object._id];
     }

     static async renderApplication(app, html, data) {
        if (Object.keys(CONFIG.ActiveJournals).find(a => a === app.object._id) === undefined) { return; }
     }
}