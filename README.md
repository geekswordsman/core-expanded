# Core Settings Expanded

Expands Foundry's Core settings and functionality, providing methods for changing defaults or adding additional features (such as Status Icons) that are not available normally available within Foundry's base settings.

Includes:
* Disabling core Status Effects, including adding custom ones.
* Overriding default angle for cone templates and ray width.
* Setting the default directory Foundry's file picker points at.
* Setting the default display mode for the file picker.
* Setting default settings when creating a new scene.
* Adding custom icons, or disabling core ones, for map notes.
* Setting default settings for creating new map notes.
* Adjust the animation speed of tokens (FVTT v10 Only!)
* Enable/Disable the Background Tint on Map Notes

# Attribution

Backgroundless Pins code based on code by @schultzcole's Backgroundless Pins module (archived at https://github.com/schultzcole/FVTT-Backgroundless-Pins)