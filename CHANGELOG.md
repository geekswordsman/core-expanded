# Core Expanded
## v1.4.2
 - Corrected issue with Map Note tint and background disabling not functioning properly in v11
 
## v1.4.1
 - Updated compatibility for v11
 - Fixed bug that could prevent updating of status effects.
 - Fixed FilePicker default settings to properly apply to FilePicker ( #5 )

## v1.4.0
 - Corrected issue with journal view modes not always working ( #13 )
 - Corrected an issue where scene settings were being applied to imported scenes, potentially causing issues with them ( # 10 )
 - Enabled the ability to edit the icons for core status effects ( # 11 )
 - Added the ability to set the default Grid Type to new scenes! ( #12 )
## v1.3.0
 - Corrected issue with Scene Defaults not all applying ( #6 )
 - Added new defaults available for opening Journals! ( #8 )
  - Can change the default view from Single View to Multiple Page View
  - Can change the default state of the sidebar to be collapsed

## v1.2.1
 - Corrected bug where background tints changes were not persisting on refresh.
 - Added new API to offer methods into the backend of Core Settings Expanded
  - Accessed via game.modules.get("core-settings-expanded").api
  - Presently only one method, MassUpdateMapNoteBackgroundTint.  See api.mjs for details.

## v1.2.0
 - Added new map note option that provides the ability to enable/disable the background tint from map notes

## v1.1.0
 - Added ability to adjust token animation speed in FVTT v10
 
## v1.0.1
 - Fixed issue where resetting Status Effects to default would not function ( #1 )
 - Corrected various translation strings
 
## v1.0.0
Initial Release! Features:
 - Login Greeting
 - Modify Status Effects
 - Modify Template Defaults
 